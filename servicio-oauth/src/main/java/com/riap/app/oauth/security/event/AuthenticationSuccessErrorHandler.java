package com.riap.app.oauth.security.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationEventPublisher;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.riap.app.commons.usuarios.models.entity.Usuario;
import com.riap.app.oauth.services.IUsuarioService;

import brave.Tracer;
import feign.FeignException;

@Component
public class AuthenticationSuccessErrorHandler implements AuthenticationEventPublisher {
	
	private final Logger logger = LoggerFactory.getLogger(AuthenticationSuccessErrorHandler.class); 

	@Autowired
	private IUsuarioService usuarioService;
	
	@Autowired
	private Tracer tracer;
	
	@Override
	public void publishAuthenticationSuccess(Authentication authentication) {
		UserDetails user = (UserDetails) authentication.getPrincipal();
		logger.info("Login success " + user.getUsername());
		
		Usuario usuario = usuarioService.findByUsername(authentication.getName());
		if (usuario.getIntentos() != null && usuario.getIntentos() > 0) {
			usuario.setIntentos(0);
			usuarioService.update(usuario, usuario.getId());
		}
	}

	@Override
	public void publishAuthenticationFailure(AuthenticationException exception, Authentication authentication) {
		logger.error("Error en authentication: " + exception.getMessage());
		try {
			StringBuilder errors = new StringBuilder();
			errors.append("Error en authentication: " + exception.getMessage());
			Usuario usuario = usuarioService.findByUsername(authentication.getName());
			if (usuario.getIntentos() == null) {
				usuario.setIntentos(0);
			}
			logger.info("Intento actual es de : " + usuario.getIntentos());
			usuario.setIntentos(usuario.getIntentos() + 1);
			logger.info("Intento despues es de : " + usuario.getIntentos());
			errors.append(" - Intento despues es de : " + usuario.getIntentos());
			if (usuario.getIntentos() >= 3) {
				String errorMaximoIntentos = String.format("Usuario %s deshabilitado por maximo de itentos", authentication.getName());
				logger.info(errorMaximoIntentos);
				errors.append(" - " + errorMaximoIntentos);
				usuario.setEnabled(Boolean.FALSE);
			}
			tracer.currentSpan().tag("error.mensaje", errors.toString());
			usuarioService.update(usuario, usuario.getId());
		} catch (FeignException e) {
			logger.error(String.format("El usuario %s no existe en el sistema", authentication.getName()));
		}
	}
	
}
