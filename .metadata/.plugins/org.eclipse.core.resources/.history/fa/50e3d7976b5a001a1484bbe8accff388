package com.riap.app.item.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.riap.app.item.model.Item;
import com.riap.app.item.model.Producto;
import com.riap.app.item.service.ItemService;

@RestController
public class ItemController {
	
	private static final Logger log = LoggerFactory.getLogger(ItemController.class);

	@Autowired
	@Qualifier("serviceRest")
	private ItemService itemService;
	
	@Value("${configuracion.texto}")
	private String texto;
	
	@GetMapping("/listar")
	public List<Item> listar() {
		return itemService.listar();
	}
	
	@HystrixCommand(fallbackMethod = "metodoAlternativo")
	@GetMapping("/ver/{id}/cantidad/{cantidad}")
	public Item obtener(@PathVariable Long id, @PathVariable Integer cantidad) {
		return itemService.obtener(id, cantidad);
	}
	
	public Item metodoAlternativo(Long id, Integer cantidad) {
		Item item = new Item();
		Producto producto = new Producto();
		producto.setId(id);
		producto.setNombre("Producto Default");
		producto.setPrecio(0.0);
		item.setProducto(producto);
		item.setCantidad(cantidad);
		return item;
	}
	
	@GetMapping("/obtener/config")
	public ResponseEntity<?> obtenerConfig(@Value("${server.port}") String puerto) {
		log.info("Texto : " + texto);
		Map<String, String> json = new HashMap<String, String>();
		json.put("texto", texto);
		json.put("puerto", puerto);
		return new ResponseEntity<Map<String, String>>(json, HttpStatus.OK);
	}
	
}
