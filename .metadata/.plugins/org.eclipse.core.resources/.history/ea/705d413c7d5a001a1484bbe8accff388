package com.riap.app.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.riap.app.model.entity.Producto;
import com.riap.app.model.service.IProductoService;

@RestController
public class ProductoController {

	@Autowired
	private Environment env;

	@Value("${server.port}")
	private Integer port;

	@Autowired
	private IProductoService productoService;

	@GetMapping("/listar")
	public List<Producto> listar() {
		return productoService.listar().stream().map(p -> {
			p.setPort(port);
			return p;
		}).collect(Collectors.toList());
	}

	@GetMapping("/ver/{id}")
	public Producto obtener(@PathVariable Long id) throws Exception {
		Producto producto = productoService.obtener(id);
		producto.setPort(Integer.parseInt(env.getProperty("local.server.port")));
		Boolean ok = true;
		if (!ok) {
			throw new Exception("No se pudo cargar el producto");
		}
		return producto;
	}
	
	@PostMapping("/crear")
	@ResponseStatus(code = HttpStatus.CREATED)
	public Producto crear(@RequestBody Producto producto) {
		return productoService.guardar(producto);
	}

}
