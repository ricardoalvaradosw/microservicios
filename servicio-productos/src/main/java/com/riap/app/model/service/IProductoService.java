package com.riap.app.model.service;

import java.util.List;

import com.riap.commons.app.models.entity.Producto;

public interface IProductoService {

	public List<Producto> listar();
	public Producto obtener(Long id);
	public Producto guardar(Producto producto);
	public void eliminar(Long id);
	
}
