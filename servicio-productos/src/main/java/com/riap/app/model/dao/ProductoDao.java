package com.riap.app.model.dao;

import org.springframework.data.repository.CrudRepository;

import com.riap.commons.app.models.entity.Producto;

public interface ProductoDao extends CrudRepository<Producto, Long> {

}
