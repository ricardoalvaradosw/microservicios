insert into usuarios (username, password, enabled, nombre, apellido, email) values ('riap', '$2a$10$wnlL8cwrJxqywSggxNAAO.jR4ORSyFavINg93vgLIk2oGmiZ7ZHm6', true,'Ricardo', 'Alvarado Aponte','ralvarado@cajasullana.pe');
insert into usuarios (username, password, enabled, nombre, apellido, email) values ('bilo', '$2a$10$8VRkWUOUIMEzQlTbWPp6e.gw/wC1eBRxF78ttdtynhJgiYZ7RlQoS', true,'Cesar', 'Bismark Lozada Madrid','clozada@cajasullana.pe');


insert into roles (nombre) values ('ROLE_USER');
insert into roles (nombre) values ('ROLE_ADMIN');

insert into usuarios_roles (usuario_id, role_id) values (1,1);
insert into usuarios_roles (usuario_id, role_id) values (2,1);
insert into usuarios_roles (usuario_id, role_id) values (1,2);