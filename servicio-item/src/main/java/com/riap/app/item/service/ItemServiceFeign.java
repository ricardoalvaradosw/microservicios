package com.riap.app.item.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.riap.app.item.clients.ProductoClientFeign;
import com.riap.app.item.model.Item;
import com.riap.commons.app.models.entity.Producto;

@Service("serviceFeign")
public class ItemServiceFeign implements ItemService {
	
	@Autowired
	private ProductoClientFeign clientFeign;

	@Override
	public List<Item> listar() {
		return clientFeign.listar().stream().map(p -> {
			Item item = new Item(p, 1);
			return item;
		}).collect(Collectors.toList());
	}

	@Override
	public Item obtener(Long id, Integer cantidad) {
		return new Item(clientFeign.obtener(id), cantidad);
	}

	@Override
	public Producto guardarProducto(Producto producto) {
		return clientFeign.crear(producto);
	}

	@Override
	public Producto actualizarProducto(Producto producto, Long id) {
		return clientFeign.actualizar(producto, id);
	}

	@Override
	public void eliminarProducto(Long id) {
		clientFeign.eliminar(id);
	}

}
