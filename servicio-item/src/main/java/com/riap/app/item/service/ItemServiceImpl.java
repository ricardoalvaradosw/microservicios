package com.riap.app.item.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.riap.app.item.model.Item;
import com.riap.commons.app.models.entity.Producto;

@Service(value = "serviceRest")
public class ItemServiceImpl implements ItemService {
	
	@Autowired
	private RestTemplate clientRest;

	@Override
	public List<Item> listar() {
		List<Producto> productos = Arrays.asList(clientRest.getForObject("http://servicio-productos/listar", Producto[].class));
		return productos.stream().map(p -> {
			Item item = new Item(p, 1);
			return item;
		}).collect(Collectors.toList());
	}

	@Override
	public Item obtener(Long id, Integer cantidad) {
		Map<String, String> parametros = new HashMap<>();
		parametros.put("id", id + "");
		Producto producto = clientRest.getForObject("http://servicio-productos/ver/{id}", Producto.class, parametros);
		return new Item(producto, cantidad);
	}

	@Override
	public Producto guardarProducto(Producto producto) {
		HttpEntity<Producto> body = new HttpEntity<Producto>(producto);
		ResponseEntity<Producto> response =  clientRest.exchange("http://servicio-productos/crear", HttpMethod.POST, body, Producto.class);
		return response.getBody();
	}

	@Override
	public Producto actualizarProducto(Producto producto, Long id) {
		Map<String, String> parametros = new HashMap<>();
		parametros.put("id", id + "");
		HttpEntity<Producto> body = new HttpEntity<Producto>(producto);
		ResponseEntity<Producto> response = clientRest.exchange("http://servicio-productos/editar/{id}", HttpMethod.PUT, body, Producto.class, parametros);
		return response.getBody();
	}

	@Override
	public void eliminarProducto(Long id) {
		Map<String, String> parametros = new HashMap<>();
		parametros.put("id", id + "");
		clientRest.delete("http://servicio-productos/eliminar/{id}", parametros);
	}

}
